package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

import javax.swing.*;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class ContractsVisitor extends VisitQuery<String> {

    PrintWriter out;
    List<String> contracts;
    List<String> conditions;
    ExprVisitor exprVisitor;
    TypeVisitor typeVisitor;
    String currentField;
    boolean fieldHandled;

    public ContractsVisitor() {
        exprVisitor = new ExprVisitor();
        typeVisitor = new TypeVisitor();
    }

    /**
     * Emit C# contracts for the given signature.
     * @param sig       target signature
     * @param out       output stream
     */
    public void emitContracts(Sig sig, PrintWriter out) {

        this.out = out;
        contracts = new ArrayList<String>();
        conditions = new ArrayList<String>();

        try {

            // Collect contracts for fields
            for (Sig.Field a : sig.getFields()) {
                currentField = a.label;
                fieldHandled = false;
                conditions.clear();

                a.decl().expr.accept(this);
                String c = GeneratorHelpers.andConditions(conditions);
                if (!c.isEmpty())
                    contracts.add(c);
            }

            // Emit contracts
            if (contracts.size() > 0) {
                out.println();
                GeneratorHelpers.indent(out, 1);
                out.println("[ContractInvariantMethod]");
                GeneratorHelpers.indent(out, 1);
                out.println("private void ObjectInvariant() {");

                for (String contract : contracts) {
                    emitContract(contract);
                }

                GeneratorHelpers.indent(out, 1);
                out.println("}");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
    }

    /**
     * Emit C# contracts for the given signature.
     * @param expr      target expression
     * @param out       output stream
     */
    public void emitContracts(Expr expr, PrintWriter out, String cField)
    {
        this.out = out;
        contracts = new ArrayList<String>();
        conditions = new ArrayList<String>();
        currentField = cField;

        try
        {
            expr.accept(this);
            contracts.add(GeneratorHelpers.andConditions(conditions));

            if (contracts.size() > 0)
                for (String contract : contracts)
                    out.print(contract.replaceAll("#", currentField));

        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
    }

    private void emitContract(String contract) {

        GeneratorHelpers.indent(out, 2);
        out.println("Contract.Invariant(" + contract.replaceAll("#", currentField) + ");");
    }

    @Override
    public String visit(Sig.Field x) throws Err {
        conditions.add(x.label + " != null");
        return "[TODO:Sig.Field]";
    }

    @Override
    public String visit(Sig x) throws Err {
        // conditions.add("[TODO:Sig]");
        return ""; //"[TODO:Sig]";
    }

    @Override
    public String visit(ExprBad x) throws Err {
        conditions.add("[TODO:ExprBad]");
        return "[TODO:ExprBad]";
    }

    @Override
    public String visit(ExprBadCall x) throws Err {
        conditions.add("[TODO:ExprBadCall]");
        return "[TODO:ExprBadCall]";
    }

    @Override
    public String visit(ExprBadJoin x) throws Err {
        conditions.add("[TODO:ExprBadJoin]");
        return "[TODO:ExprBadJoin]";
    }

    private String getRelation(Expr node) {

        String relation = "[ERROR]";
        try {
            // 0 = Set, 1 = e.ItemX
            String type = node.accept(typeVisitor);
            if (type.length() >= 3 && type.substring(0, 3).contentEquals("ISet")) {
                relation = "{0}.Contains({1})";
            } else if (GeneratorHelpers.isSig(node)) {
                relation = "{1} is {0}";
            } else {
                relation = "{0}.Equals({1})";
            }
        }
        catch (Exception e) {
            System.out.println("Error. Unable to get relation. " + e.getMessage());
        }

        return relation;
    }

    void addElementsConstraints(ExprBinary x) {
        try {
            String leftRelation = getRelation(x.left);
            String rightRelation = getRelation(x.right);

            conditions.add("Contract.ForAll(#, t => t != null && "
                    + MessageFormat.format(leftRelation, x.left.accept(exprVisitor), "t.Item1")
                    + ")");
            conditions.add("Contract.ForAll(#, t => t != null && "
                    + MessageFormat.format(rightRelation, x.right.accept(exprVisitor), "t.Item2")
                    + ")");
        }
        catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    void commonChecks(ExprBinary x) {
        try {
            if (!fieldHandled) {
                fieldHandled = true;
                conditions.add(currentField + " != null");
            }

            x.left.accept(this);
            x.right.accept(this);
        }
        catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    String sourceSet(Expr node) {

        String source = "[ERROR]";
        try {
            // 0 = Set, 1 = e.ItemX
            String type = node.accept(typeVisitor);

            Expr e = node;
            while (e instanceof ExprUnary && ((((ExprUnary) e).op == ExprUnary.Op.NOOP) ||
                    (((ExprUnary) e).op == ExprUnary.Op.ONEOF) || (((ExprUnary) e).op == ExprUnary.Op.LONEOF))) {
                e = ((ExprUnary) e).sub;

                if (type.length() >= 3 && type.substring(0, 3).contentEquals("ISet"))
                    source = node.accept(exprVisitor);
                else
                    source = "(new HashSet<" + type + ">(new " + type + "[] { " + node.accept(exprVisitor) + " }))";
            }

            if (e instanceof ExprVar) {
            } else {
                source = "#";
            }
        }
        catch (Exception e) {
            System.out.println("Error. Unable to get source set: " + e.getMessage());
        }

        return source;
    }

    @Override
    public String visit(ExprBinary x) throws Err {

        switch (x.op) {
            case ARROW:
                commonChecks(x);
                addElementsConstraints(x);
                // ANY to ANY: No other constraints on the content of the touples
                break;
            case ANY_ARROW_SOME:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() >= 1)");
                break;
            case ANY_ARROW_ONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() == 1)");
                break;
            case ANY_ARROW_LONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() <= 1)");
                break;
            case SOME_ARROW_ANY:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() >= 1)");
                break;
            case SOME_ARROW_SOME:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() >= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() >= 1)");
                break;
            case SOME_ARROW_ONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() == 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() >= 1)");
                break;
            case SOME_ARROW_LONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() <= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() >= 1)");
                break;
            case ONE_ARROW_ANY:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() == 1)");
                break;
            case ONE_ARROW_SOME:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() >= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() == 1)");
                break;
            case ONE_ARROW_ONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() == 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() == 1)");
                break;
            case ONE_ARROW_LONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() <= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() == 1)");
                break;
            case LONE_ARROW_ANY:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() <= 1)");
                break;
            case LONE_ARROW_SOME:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() >= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() <= 1)");
                break;
            case LONE_ARROW_ONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() == 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() <= 1)");
                break;
            case LONE_ARROW_LONE:
                commonChecks(x);
                addElementsConstraints(x);
                conditions.add("Contract.ForAll(" + sourceSet(x.left) + " , e => e != null && #.Where(t => t != null && t.Item1 == e.Item1).Count() <= 1)");
                conditions.add("Contract.ForAll(" + sourceSet(x.right) + " , e => e != null && #.Where(t => t != null && t.Item2 == e.Item2).Count() <= 1)");
                break;
            case ISSEQ_ARROW_LONE:
                conditions.add("[NOT_REQUIRED:ExprBinary:ISSEQ_ARROW_LONE]");
                break;
            case JOIN:
                x.right.accept(this);
                // As said in the emails there is no need to check for null dereferencing
                break;
            case DOMAIN:
                conditions.add("[NOT_REQUIRED:ExprBinary:DOMAIN]");
                break;
            case RANGE:
                conditions.add("[NOT_REQUIRED:ExprBinary:RANGE]");
                break;
            case INTERSECT:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case PLUSPLUS:
                conditions.add("[NOT_REQUIRED:ExprBinary:PLUSPLUS]");
                break;
            case PLUS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case IPLUS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case MINUS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case IMINUS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case MUL:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case DIV:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case REM:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case EQUALS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_EQUALS:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case IMPLIES:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case LT:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case LTE:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case GT:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case GTE:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_LT:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_LTE:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_GT:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_GTE:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case SHL:
                conditions.add("[NOT_REQUIRED:ExprBinary:SHL]");
                break;
            case SHA:
                conditions.add("[NOT_REQUIRED:ExprBinary:SHA]");
                break;
            case SHR:
                conditions.add("[NOT_REQUIRED:ExprBinary:SHR]");
                break;
            case IN:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case NOT_IN:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case AND:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case OR:
                x.left.accept(this);
                x.right.accept(this);
                break;
            case IFF:
                x.left.accept(this);
                x.right.accept(this);
                break;
        }

        return "[TODO:ExprBinary:???]";
    }

    @Override
    public String visit(ExprConstant x) throws Err {

        // Constants have no condition
        //switch (x.op) {
        //    case TRUE:
        //        conditions.add("[TODO:ExprConstant:TRUE]");
        //        break;
        //    case FALSE:
        //        conditions.add("[TODO:ExprConstant:FALSE]");
        //        break;
        //    case IDEN:
        //        conditions.add("[NOT_REQUIRED:ExprConstant:IDEN]");
        //        break;
        //    case MIN:
        //        conditions.add("[TODO:ExprConstant:MIN]");
        //        break;
        //    case MAX:
        //        conditions.add("[TODO:ExprConstant:MAX]");
        //        break;
        //    case NEXT:
        //        conditions.add("[NOT_REQUIRED:ExprConstant:NEXT]");
        //        break;
        //    case EMPTYNESS:
        //        conditions.add("[NOT_REQUIRED:ExprConstant:EMPTYNESS]");
        //        break;
        //    case STRING:
        //        conditions.add("[TODO:ExprConstant:STRING]");
        //        break;
        //    case NUMBER:
        //        conditions.add("[TODO:ExprConstant:NUMBER]");
        //        break;
        //}

        return "[TODO:ExprConstant]";
    }

    @Override
    public String visit(ExprITE x) throws Err {
        x.cond.accept(this);
        x.left.accept(this);
        x.right.accept(this);
        // No conditions, just accept the children
        return "[TODO:ExprITE]";
    }

    @Override
    public String visit(ExprLet x) throws Err {
        x.sub.accept(this);
        // Simple text substitution.. no conditions
        return "[TODO:ExprLet]";
    }

    @Override
    public String visit(ExprQt x) throws Err {
        x.sub.accept(this);
        conditions.add("[NOT_REQUIRED:ExprQt] ");
        return "[NOT_REQUIRED:ExprQt]";
    }

    @Override
    public String visit(ExprUnary x) throws Err {
        switch (x.op) {
            case SOMEOF:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                conditions.add(currentField + ".Count >= 1");
                break;
            case LONEOF:
                x.sub.accept(this);
                // Can be one or null. No constraints.
                break;
            case ONEOF:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case SETOF:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case EXACTLYOF:
                x.sub.accept(this);
                conditions.add("[NOT_REQUIRED:ExprUnary:EXACTLYOF]");
                break;
            case NOT:
                x.sub.accept(this);
                // Logic operations: no constraints
                break;
            case NO:
                x.sub.accept(this);
                conditions.add("[NOT_REQUIRED:ExprUnary:NO]");
                break;
            case SOME:
                x.sub.accept(this);
                conditions.add("[NOT_REQUIRED:ExprUnary:SOME]");
                break;
            case LONE:
                x.sub.accept(this);
                conditions.add("[NOT_REQUIRED:ExprUnary:LONE]");
                break;
            case ONE:
                x.sub.accept(this);
                conditions.add("[NOT_REQUIRED:ExprUnary:ONE]");
                break;
            case TRANSPOSE:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case RCLOSURE:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case CLOSURE:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case CARDINALITY:
                x.sub.accept(this);
                conditions.add(currentField + " != null");
                break;
            case CAST2INT:
                x.sub.accept(this);
                // Arithmetic operations: no constraints
                break;
            case CAST2SIGINT:
                x.sub.accept(this);
                // Arithmetic operations: no constraints
                break;
            case NOOP:
                x.sub.accept(this);
                break;
        };

        return "[TODO:ExprUnary:???]";
    }

    @Override
    public String visit(ExprVar x) throws Err {

        return "[TODO:ExprVar]";
    }
}