package edu.mit.csail.sdg.alloy4compiler.generator;

import java.io.PrintWriter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.SafeList;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.PrimSig;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.SubsetSig;

public class CodeGeneratorVisitor extends VisitQuery<Object> {
	
	PrintWriter out;
	Iterable<Sig> sigs;
    TypeVisitor typeVisitor;
    ExprVisitor exprVisitor;
    ContractsVisitor contractsVisitor;
	
	public void generate(PrintWriter out, Iterable<Sig> sigs, SafeList<Func> funcs) {
		this.out = out;
		this.sigs = sigs;

        typeVisitor = new TypeVisitor();
        exprVisitor = new ExprVisitor();
        contractsVisitor = new ContractsVisitor();

		try {
			for (Sig sig : sigs) {
				sig.accept(this);
			}
			
			emitFuncs(funcs);
			
	  	} catch (Exception e) {
	  		System.out.println("Error: " + e.getMessage());
	  	}
	}

	public CodeGeneratorVisitor() {

        out = null;
	}

    /**
     * Emit C# code for a signature declaration. This is intended for declarations only,
     * when accessing signatures as type expression TypeVisitor should be used.
     *
     * @param x     target signature
     * @throws Err
     */
	@Override
	public Object visit(Sig x) throws Err {

		if (!x.builtin) {
			
			// Build modifiers
			String modifiers = "";
			if (x.isAbstract != null)
				modifiers += "abstract ";

            modifiers += "public ";

            // Emit declaration
			out.print(modifiers + "class " + GeneratorHelpers.removeThisPrefix(x.label));
			
			// Emit extends
			if (x.isSubsig != null) {
				PrimSig primSig = (PrimSig)x;
				
				if (primSig.parent != null) {
					String sigName = GeneratorHelpers.removeThisPrefix(primSig.parent.label);
					
					if (!sigName.contentEquals("univ"))
						out.print(" : " + sigName);
				}
			} else {
				throw new RuntimeException("Subset signatures not handled!");
			}
			out.print(" {" + System.lineSeparator());

            // Emit code for "one" signatures
            if (x.isOne != null)
                emitSingleton(out, x);

			// Emit fields
			for (Field a : x.getFields()) {
				a.accept(this);
			}

            // Emit contracts
            contractsVisitor.emitContracts(x, out);

			// Close class declaration
			out.println("}");
			out.println();
		}
		
		return null;
	}

    public void emitSingleton(PrintWriter out, Sig sig) {

        String name = GeneratorHelpers.removeThisPrefix(sig.label);

        GeneratorHelpers.indent(out, 1);
        out.println("private static " + name + " instance;");
        GeneratorHelpers.indent(out, 1);
        out.println("private " + name + "() { }");
        GeneratorHelpers.indent(out, 1);
        out.println("public static " + name + " Instance {");
        GeneratorHelpers.indent(out, 2);
        out.println(    "get {");
        GeneratorHelpers.indent(out, 3);
        out.println(        "if (instance == null) {");
        GeneratorHelpers.indent(out, 4);
        out.println(            "instance = new " + name + "();");
        GeneratorHelpers.indent(out, 3);
        out.println(        "}");
        GeneratorHelpers.indent(out, 3);
        out.println(        "return instance;");
        GeneratorHelpers.indent(out, 2);
        out.println(    "}");
        GeneratorHelpers.indent(out, 1);
        out.println("}");
    }

    /**
     * Emit code for signature fields
     *
     * @param x     target field.
     * @throws Err
     */
	@Override
	public Object visit(Field x) throws Err {
		
		String modifier = "public";

        String type = x.decl().expr.accept(typeVisitor);
		String name = x.label;

		GeneratorHelpers.indent(out, 1);
		out.println(modifier + " " + type + " " + name + ";");

        return null;
	}

	/**
	 * Emits functions and predicates.
	 * 
	 * @param funcs		the list of functions and predicates to be emitted.
	 */
	private void emitFuncs(SafeList<Func> funcs) throws Err
    {
        // Emit only if there is any functions.
        if (funcs.size() > 0)
        {
            // Emit functions class declaration.
            out.println("public static class FuncClass {");

            // Iterate through all functions and predicates.
            for (Func func : funcs)
            {
                // Emit predicate.
                if (func.isPred)
                {
                    // Emit predicate declaration.
                    GeneratorHelpers.indent(out, 1);
                    out.print("public static bool " + GeneratorHelpers.removeThisPrefix(func.label));

                    // Emit predicate parameters.
                    StringBuilder builder = new StringBuilder();
                    for (Decl decl : func.decls)
                    {
                        for (ExprHasName name : decl.names)
                        {
                            if (builder.length() != 0)
                                builder.append(", ");

                            builder.append(decl.expr.accept(typeVisitor) + " ");
                            builder.append(name.accept(exprVisitor));
                        }
                    }
                    out.println("(" + builder.toString() + ") {");

                    // Emit predicate body.
                    GeneratorHelpers.indent(out, 2);
                    out.print("return ");
                    out.print(func.getBody().accept(exprVisitor));
                    out.println(";");

                    // Close predicate declaration.
                    GeneratorHelpers.indent(out, 1);
                    out.println("}");
                }

                // Emit function.
                else
                {
                    // Emit function declaration.
                    GeneratorHelpers.indent(out, 1);
                    out.print("public static ");

                    // Emit function return type.
                    out.print(func.returnDecl.accept(typeVisitor) + " ");

                    // Emit function name.
                    out.print(GeneratorHelpers.removeThisPrefix(func.label));

                    // Emit function parameters.
                    StringBuilder builder = new StringBuilder();
                    for (Decl decl : func.decls)
                    {
                        for (ExprHasName name : decl.names)
                        {
                            if (builder.length() != 0)
                                builder.append(", ");

                            builder.append(decl.expr.accept(typeVisitor) + " ");
                            builder.append(name.accept(exprVisitor));
                        }
                    }
                    out.println("(" + builder.toString() + ") {");

                    // Emit function contracts.
                    for (Decl decl : func.decls)
                    {
                        for (ExprHasName name : decl.names)
                        {
                            GeneratorHelpers.indent(out, 2);
                            out.println("Contract.Requires(" + name.label + " != null);");
                        }
                    }
                    GeneratorHelpers.indent(out, 2);
                    out.print("Contract.Ensures(");
                    contractsVisitor.emitContracts(func.returnDecl, out, "Contract.Result<" + func.returnDecl.accept(typeVisitor) + ">()");
                    out.println(");");

                    // Emit function return.
                    GeneratorHelpers.indent(out, 2);
                    out.print("return ");
                    out.print(func.getBody().accept(exprVisitor));
                    out.println(";");

                    // Close function declaration.
                    GeneratorHelpers.indent(out, 1);
                    out.println("}");
                }
            }

            // Close class declaration.
            out.println("}");
            out.println();
        }

        // If needed, emit helper class for closure.
        if (exprVisitor.getMustEmitHelperClass())
            out.print(ExprVisitor.getHelperClass());
	}
}
