sig Person {
  spouse: lone Person
}

assert symmetricMariage {
  spouse = ~spouse
}

check symmetricMariage for 2
