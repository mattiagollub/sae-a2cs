package edu.mit.csail.sdg.alloy4compiler.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.Pair;
import edu.mit.csail.sdg.alloy4.ErrorFatal;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Tuple;

public final class TestGenerator
{
    private ExprVisitor exprVisitor = new ExprVisitor();
  
    private TestGenerator(A4Solution solution, Iterable<Sig> sigs, Iterable<Pair<String, Expr>> assertions, Iterable<Command> cmds, String originalFilename, PrintWriter out) throws Err
    {
        emitHeader(out, originalFilename);
        GeneratorHelpers.indent(out, 2);
        out.println("// setup test data");
  
        for (Sig sig : sigs) 
        {
            if (!sig.builtin) //check wheter the sig is a builtin sig or not
            {
                String signame = GeneratorHelpers.removeThisPrefix(sig.toString());
                String setname = signame + "Set";
                GeneratorHelpers.indent(out, 2);
                out.println("var " + setname + " = new HashSet<" + signame + ">();");
                if (!solution.eval(sig).toString().equals("{}"))
                {
                    for (A4Tuple tuple : solution.eval(sig))
                    {
                        signame = GeneratorHelpers.unbracket(tuple.toString(), '{', '}').split("\\$")[0];
                        String en = GeneratorHelpers.unbracket(tuple.toString().replaceAll("\\$", ""), '{', '}');
                        String[] entitynames = en.split(", ");
                        for (int i = 0; i < entitynames.length; i++)
                        {
                            if (sig.isTopLevel()) //check whether the sig is toplevel
                            {
                                GeneratorHelpers.indent(out, 2);
                                out.println(signame + " " + en + ";");
                                GeneratorHelpers.indent(out, 2);
                                if (tuple.sig(0).isOne != null || tuple.sig(0).isLone != null)
                                    out.println(setname + ".Add(" + en + " = " + signame + ".Instance);");
                                else
                                    out.println(setname + ".Add(" + en + " = new " + signame + "());");
                            }
                            else //the sig isn't abstract and isn't toplevel
                            {       
                                String entityname = entitynames[i];
                                GeneratorHelpers.indent(out, 2);
                                out.println(setname + ".Add(" + entityname +");");                          
                            }
                        }
                    }
                    List<String> listentities = new ArrayList<String>();
                    for (Field field:sig.getFields())
                    {
                        if (solution.eval(field).size() != 0)
                        {
                            for (A4Tuple tuple:solution.eval(field))
                            {
                                if(field.decl().expr.toString().split(" ")[0].equals("one") || field.decl().expr.toString().split(" ")[0].equals("lone"))
                                {
                                    GeneratorHelpers.indent(out, 2);
                                    out.println(tuple.toString().replaceAll("->", "." + field.label + " = ").replaceAll("\\$", "") + ";");
                                }
                                else
                                {
                                    String[] entities = tuple.toString().split("->");
                                    GeneratorHelpers.indent(out, 2);
                                    String entityfield = entities[0].replaceAll("\\$", "") + "." + field.label;
                                    if (entities.length < 3)
                                    {
                                        if (!listentities.contains(entityfield))
                                        {
                                            if (field.type().arity() == 1)
                                                out.println(entityfield + " = new HashSet<" + GeneratorHelpers.removeThisPrefix(GeneratorHelpers.unbracket(field.type().pickUnary().toString(), '{', '}')) + ">();");
                                            else
                                                out.println(entityfield + " = new HashSet<" + GeneratorHelpers.removeThisPrefix(GeneratorHelpers.unbracket(field.type().pickBinary().toString(), '{', '}').split("->")[1]) + ">();");
                                            listentities.add(entityfield);
                                            GeneratorHelpers.indent(out, 2);
                                        }
                                        out.println(entityfield + ".Add(" + entities[1].replaceAll("\\$", "") + ");");
                                    }
                                    else
                                    {
                                        if (!listentities.contains(entityfield))
                                        {
                                            out.println(entityfield + " = new HashSet<Tuple<" + entities[1].split("\\$")[0] + "," + entities[2].split("\\$")[0] + ">>();");
                                            listentities.add(entityfield);
                                            GeneratorHelpers.indent(out, 2);
                                        }
                                        out.println(entityfield + ".Add(Tuple.Create(" + entities[1].replaceAll("\\$", "") + ", " + entities[2].replaceAll("\\$", "") + "));");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        out.println();
        GeneratorHelpers.indent(out, 2);
        out.println("// check test data");
 
        for (Command cmd : cmds)
            for (Pair<String, Expr> ass : assertions)
                if (ass.a.toString().equals(cmd.label))
                {
                    GeneratorHelpers.indent(out, 2);
                    out.println("Contract.Assert(" + ass.b.accept(exprVisitor) + ", \"" + ass.a + "\");");
                }
  
        GeneratorHelpers.indent(out, 1);
        out.println("}");
        out.println("}");
        if (exprVisitor.getMustEmitHelperClass())
        {
            out.println();
            out.print(ExprVisitor.getHelperClass());
        }
        out.close();
  
    } 
  
    private void emitHeader(PrintWriter out, String originalFileName)
    {
        out.println("// This C# file is generated from " + originalFileName);
        out.println();
        out.println("using System;");
        out.println("using System.Linq;");
        out.println("using System.Collections.Generic;");
        out.println("using System.Diagnostics.Contracts;");
        out.println();
        out.println("public static class Test {");
        GeneratorHelpers.indent(out, 1);
        out.println("public static void Main(string[] args) {");
    }

    public static void writeTest(A4Solution solution, Module world, String originalFilename, boolean saveInDist) throws Err
    {
        try
        {
            String f;
            String ext = ".tests.cs";
            if (saveInDist)
            {
                f = ".\\" + new File(originalFilename).getName() + ext;
            }   
            else
            {
                f = originalFilename + ext;
            }
            File file = new File(f);
            if (file.exists())
            {
                file.delete();
            }
            PrintWriter out = new PrintWriter(new FileWriter(f, true));
            new TestGenerator(solution, world.getAllReachableSigs(), world.getAllAssertions(), world.getAllCommands(), originalFilename, out);
        }
        catch (Throwable ex)
        {
            if (ex instanceof Err)
            {
                throw (Err)ex;
            }
            else
            {
                throw new ErrorFatal("Error writing the generated C# test file.", ex);
            }
        }
    }
}
