open util/ordering[FileSystem]

abstract sig FSObject {}

sig File, Dir extends FSObject {}

sig FileSystem {
  live: set FSObject,
  root: Dir & live,
  parent: (live - root) -> one (Dir & live),
  contents: (Dir & live) -> live
}

pred inv[ s: FileSystem ] {
  // A directory is the parent of its contents and vice versa
  s.contents = ~(s.parent)

  // File system is connected
  s.live in s.root.*(s.contents)
}

pred init[ s: FileSystem ] {
  #s.live = 1 
  s.contents[s.root] = none
} 

pred add[ s, s': FileSystem, o: FSObject, d: Dir ] {
  !(o in s.live) && d in s.live &&
  s'.live = s.live + o
  s'.parent = s.parent + (o->d)
  s'.contents = s.contents ++ (d->(s.contents[d] + o))
}  

pred removeAll[ s, s': FileSystem, o: FSObject ] {
  o in s.live - s.root &&
  s'.live = s.live - o.*(s.contents) &&
  s'.parent = s'.live <: s.parent &&
  s'.contents = s.contents :> s'.live
}

fact traces {
  init[first] &&
  all s: FileSystem - last | 
    (some o: FSObject | removeAll[s, s.next, o])  ||
    (some o: FSObject, d: Dir | add[s, s.next, o, d])
}

assert invHolds {
  all s: FileSystem | inv[s]
}
check invHolds

assert invtemp {
  all s, s': FileSystem | s.root = s'.root //&&s'.live in s.live
}
check invtemp 
