package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

public class ExprVisitor extends VisitQuery<String>
{
    /**
     * Type visitor needed when creating new instances.
     */
    private TypeVisitor typeVisitor;

    /**
     * Flag indicationg whether the helper functions class is required to be emitted.
     */
    private boolean mustEmitHelperClass;

    /**
     * Defaul constructor.
     */
    public ExprVisitor()
    {
        typeVisitor = new TypeVisitor();
        mustEmitHelperClass = false;
    }

    /**
     * Emit an error string for a ExprBad.
     *
     * @param x     The ExprBad object.
     * @return      An error string for a ExprBad.
     * @throws Err
     */
    @Override
    public String visit(ExprBad x) throws Err
    {
        return "[BAD_EXPRESSION]";
    }

    /**
     * Emit an error string for a ExprBadCall.
     *
     * @param x     The ExprBadCall object.
     * @return      An error string for a ExprBadCall.
     * @throws Err
     */
    @Override
    public String visit(ExprBadCall x) throws Err
    {
        return "[BAD_CALL]";
    }

    /**
     * Emit an error string for a ExprBadJoin.
     *
     * @param x     The ExprBadJoin object.
     * @return      An error string for a ExprBadJoin.
     * @throws Err
     */
    @Override
    public String visit(ExprBadJoin x) throws Err
    {
        return "[BAD_JOIN]";
    }

    /**
     * Emit a string representation of the C# code for a ExprBinary.
     *
     * @param x     The ExprBinary object.
     * @return      A string representing the equivalent C# code of the ExprBinary.
     * @throws Err
     */
    @Override
    public String visit(ExprBinary x) throws Err
    {
        switch (x.op)
        {
            case ARROW:
            case ANY_ARROW_SOME:
            case ANY_ARROW_ONE:
            case ANY_ARROW_LONE:
            case SOME_ARROW_ANY:
            case SOME_ARROW_SOME:
            case SOME_ARROW_ONE:
            case SOME_ARROW_LONE:
            case ONE_ARROW_ANY:
            case ONE_ARROW_SOME:
            case ONE_ARROW_ONE:
            case ONE_ARROW_LONE:
            case LONE_ARROW_ANY:
            case LONE_ARROW_SOME:
            case LONE_ARROW_ONE:
            case LONE_ARROW_LONE:
                String exprL = x.left.accept(this);
                String exprR = x.right.accept(this);
                String typeL = x.left.accept(typeVisitor);
                String typeR = x.right.accept(typeVisitor);
                String innerType = "Tuple<" + typeL + ", " + typeR + ">";
                String outerType = "HashSet<" + innerType + ">";
                return "new " + outerType + "(new " + innerType + "(" + exprL + ", " + exprR + "))";
            case ISSEQ_ARROW_LONE:
                return "[NOT_REQUIRED: ExprBinary.Op.ISSEQ_ARROW_LONE]";
            case JOIN:
                String left = x.left.accept(this);
                return "(" + (left.equals("this") ? "" : left + ".") + x.right.accept(this) + ")";
            case DOMAIN:
                return "[NOT_REQUIRED: ExprBinary.Op.DOMAIN]";
            case RANGE:
                return "[NOT_REQUIRED: ExprBinary.Op.RANGE]";
            case INTERSECT:
                if (GeneratorHelpers.isSig(x.left) && GeneratorHelpers.isSig(x.right))
                    return "[NOT_IMPLEMENTED: Intersect between signatures]";
                if (GeneratorHelpers.isSig(x.left))
                    return "(new HashSet<" + x.right.accept(typeVisitor) + ">(" +  x.right.accept(this) + ".Where(t2 => t2 is " + x.left.accept(this) + ")))";
                if (GeneratorHelpers.isSig(x.right))
                    return "(new HashSet<" + x.left.accept(typeVisitor) + ">(" +  x.left.accept(this) + ".Where(t2 => t2 is " + x.right.accept(this) + ")))";
                return "(new HashSet<" + x.left.accept(typeVisitor) + ">(" +  x.left.accept(this) + ".Intersect(" + x.right.accept(this) + ")))";
            case PLUSPLUS: // Union WITHOUT duplicates.
            case PLUS: // Union WITH duplicates. (!)
                return "(new HashSet<" + x.left.accept(typeVisitor) + ">(" + x.left.accept(this) + ".Union(" + x.right.accept(this) + ")))";
            case IPLUS:
                return "(" + x.left.accept(this) + " + " + x.right.accept(this) + ")";
            case MINUS:
                if (GeneratorHelpers.isSig(x.left) || GeneratorHelpers.isSig(x.right))
                    return "[NOT_IMPLEMENTED:Subtraction between signatures]";

                return "(new HashSet<" + x.left.accept(typeVisitor) + ">(" + x.left.accept(this) + ".Except(" + sourceSet(x.right) + ")))";
            case IMINUS:
                return "(" + x.left.accept(this) + " - " + x.right.accept(this) + ")";
            case MUL:
                return "(" + x.left.accept(this) + " * " + x.right.accept(this) + ")";
            case DIV:
                return "(" + x.left.accept(this) + " / " + x.right.accept(this) + ")";
            case REM:
                return "(" + x.left.accept(this) + " % " + x.right.accept(this) + ")";
            case EQUALS:
                return "(" + x.left.accept(this) + ".Equals(" + x.right.accept(this) + "))";
            case NOT_EQUALS:
                return "!(" + x.left.accept(this) + ".Equals(" + x.right.accept(this) + "))";
            case IMPLIES:
                return "(!" + x.left.accept(this) + " || " + x.right.accept(this) + ")";
            case LT:
                return "(" + x.left.accept(this) + " < " + x.right.accept(this) + ")";
            case LTE:
                return "(" + x.left.accept(this) + " <= " + x.right.accept(this) + ")";
            case GT:
                return "(" + x.left.accept(this) + " > " + x.right.accept(this) + ")";
            case GTE:
                return "(" + x.left.accept(this) + " >= " + x.right.accept(this) + ")";
            case NOT_LT:
                return "!(" + x.left.accept(this) + " < " + x.right.accept(this) + ")";
            case NOT_LTE:
                return "!(" + x.left.accept(this) + " <= " + x.right.accept(this) + ")";
            case NOT_GT:
                return "!(" + x.left.accept(this) + " > " + x.right.accept(this) + ")";
            case NOT_GTE:
                return "!(" + x.left.accept(this) + " >= " + x.right.accept(this) + ")";
            case SHL:
                return "(" + x.left.accept(this) + " << " + x.right.accept(this) + ")";
            case SHA:
            case SHR:
                return "(" + x.left.accept(this) + " >> " + x.right.accept(this) + ")";
            case IN:
                if (GeneratorHelpers.isSig(x.right) && GeneratorHelpers.isSig(x.left))
                    return "(typeof(" + x.left.accept(this) + ").IsSubclassOf(typeof(" + x.right.accept(this) + ")))";
                if (GeneratorHelpers.isSig(x.right))
                    return "(" + x.left.accept(this) + " is " + x.right.accept(this) + ")";
                return "(" + x.right.accept(this) + ".Contains(" + x.left.accept(this) + "))";
            case NOT_IN:
                if (GeneratorHelpers.isSig(x.right) && GeneratorHelpers.isSig(x.left))
                    return "!(typeof(" + x.left.accept(this) + ").IsSubclassOf(typeof(" + x.right.accept(this) + ")))";
                if (GeneratorHelpers.isSig(x.right))
                    return "!(" + x.left.accept(this) + " is " + x.right.accept(this) + ")";
                return "!(" + x.right.accept(this) + ".Contains(" + x.left.accept(this) + "))";
            case AND:
                return "(" + x.left.accept(this) + " && " + x.right.accept(this) + ")";
            case OR:
                return "(" + x.left.accept(this) + " || " + x.right.accept(this) + ")";
            case IFF:
                return "(" + x.left.accept(this) + " == " + x.right.accept(this) + ")";
            default:
                return "[NOT_AN_OP: ExprBinary]";
        }
    }

    /**
     * Emit a string representation of the C# code for a ExprCall.
     *
     * @param x     The ExprCall object.
     * @return      A string representing the equivalent C# code of the ExprCall.
     * @throws Err
     */
    @Override
    public String visit(ExprCall x) throws Err
    {
        String funcName = GeneratorHelpers.removeThisPrefix(x.fun.label);

        // Get function parameters.
        StringBuilder builder = new StringBuilder();
        for (Decl decl : x.fun.decls)
        {
            for (ExprHasName name : decl.names)
            {
                if (builder.length() != 0)
                    builder.append(", ");

                builder.append(name.label);
            }
        }
        String funcArgs = "(" + builder.toString() + ")";

        return "FuncClass." + funcName + funcArgs;
    }

    /**
     * Emit a string representation of the C# code for a ExprConstant.
     *
     * @param x     The ExprConstant object.
     * @return      A string representing the equivalent C# code of the ExprConstant.
     * @throws Err
     */
    @Override
    public String visit(ExprConstant x) throws Err
    {
        switch (x.op)
        {
            case TRUE:
                return "true";
            case FALSE:
                return "false";
            case IDEN:
                return "[NOT_REQUIRED: ExprConstant.Op.IDEN]";
            case MIN:
                return "int.MinValue";
            case MAX:
                return "int.MaxValue";
            case NEXT:
                return "[NOT_REQUIRED: ExprConstant.Op.NEXT]";
            case EMPTYNESS:
                return "[NOT_REQUIRED: ExprConstant.Op.EMPTYNESS]";
            case STRING:
                return x.string;
            case NUMBER:
                return Integer.toString(x.num);
            default:
                return "[NOT_AN_OP: ExprConstant]";
        }
    }

    /**
     * Emit a string representation of the C# code for a ExprITE.
     *
     * @param x     The ExprITE object.
     * @return      A string representing the equivalent C# code of the ExprITE.
     * @throws Err
     */
    @Override
    public String visit(ExprITE x) throws Err
    {
        return "(" + x.cond.accept(this) + " ? " +
                x.left.accept(this) + " : " +
                x.right.accept(this) + ")";
    }

    /**
     * Emit a string representation of the C# code for a ExprLet.
     *
     * @param x     The ExprLet object.
     * @return      A string representing the equivalent C# code of the ExprLet.
     * @throws Err
     */
    @Override
    public String visit(ExprLet x) throws Err
    {
        String exprType = x.expr.accept(typeVisitor);
        String varType = x.var.accept(typeVisitor);

        return "((new Func<" + varType + ", " + exprType + ">((" +
                x.var.label + ") => { return " + x.sub.accept(this) +
                "; }))(" + x.expr.accept(this) + "))";
    }

    /**
     * Emit a string representation of the C# code for a ExprList.
     *
     * @param x     The ExprList object.
     * @return      A string representing the equivalent C# code of the ExprList.
     * @throws Err
     */
    @Override
    public String visit(ExprList x) throws Err
    {
        String op;

        switch (x.op)
        {
            case DISJOINT:
                return "[NOT_REQUIRED: ExprList.Op.DISJOINT]";
            case TOTALORDER:
                return "[NOT_REQUIRED: ExprList.Op.TOTALORDER]";
            case AND:
                op = " && ";
                break;
            case OR:
                op = " || ";
                break;
            default:
                return "[NOT_AN_OP: ExprList]";
        }

        StringBuilder builder = new StringBuilder();
        for (Expr arg : x.args)
        {
            if (builder.length() != 0)
                builder.append(op);

            builder.append(arg.accept(this));
        }

        return builder.toString();
    }

    /**
     * Emit a string representation of the C# code for a ExprQt.
     *
     * @param x     The ExprQt object.
     * @return      A string representing the equivalent C# code of the ExprQt.
     * @throws Err
     */
    @Override
    public String visit(ExprQt x) throws Err
    {
        String setName;
        String varName;

        switch (x.op)
        {
            case ALL:
                StringBuilder builder = new StringBuilder();
                for (Decl decl : x.decls)
                {
                    for (ExprHasName name : decl.names)
                    {
                        setName = name.accept(typeVisitor) + "Set";
                        varName = name.accept(this);
                        builder.append("Contract.ForAll(" + setName + ", " + varName + " => ");
                    }
                }
                builder.append(x.sub.accept(this));
                for (Decl decl : x.decls)
                    for (int i = 0; i < decl.names.size(); ++i)
                        builder.append(")");
                return builder.toString();
            case COMPREHENSION:
                return "[NOT_REQUIRED: ExprBinary.Op.COMPREHENSION]";
            case LONE:
                setName = x.get(0).accept(typeVisitor) + "Set";
                varName = x.get(0).accept(this);
                return "(" + setName + ".Where(" + varName + " => " + x.sub.accept(this) + ").Count() <= 1)";
            case NO:
                setName = x.get(0).accept(typeVisitor) + "Set";
                varName = x.get(0).accept(this);
                return "(" + setName + ".Where(" + varName + " => " + x.sub.accept(this) + ").Count() == 0)";
            case ONE:
                setName = x.get(0).accept(typeVisitor) + "Set";
                varName = x.get(0).accept(this);
                return "(" + setName + ".Where(" + varName + " => " + x.sub.accept(this) + ").Count() == 1)";
            case SOME:
                setName = x.get(0).accept(typeVisitor) + "Set";
                varName = x.get(0).accept(this);
                return "(" + setName + ".Where(" + varName + " => " + x.sub.accept(this) + ").Count() >= 1)";
            case SUM:
                return "[NOT_REQUIRED: ExprBinary.Op.SUM]";
            default:
                return "[NOT_AN_OP: ExprBinary]";
        }
    }

    /**
     * Emit a string representation of the C# code for a ExprUnary.
     *
     * @param x     The ExprUnary object.
     * @return      A string representing the equivalent C# code of the ExprUnary.
     * @throws Err
     */
    @Override
    public String visit(ExprUnary x) throws Err
    {
        switch (x.op)
        {
            case SOMEOF:
                return x.sub.accept(this);
            case LONEOF:
                return x.sub.accept(this);
            case ONEOF:
                return x.sub.accept(this);
            case SETOF:
                return x.sub.accept(this);
            case EXACTLYOF:
                return x.sub.accept(this);
            case NOT:
                return "!" + x.sub.accept(this);
            case NO:
                return "[NOT_REQUIRED: ExprUnary.Op.NO]";
            case SOME:
                return "[NOT_REQUIRED: ExprUnary.Op.SOME]";
            case LONE:
                return "[NOT_REQUIRED: ExprUnary.Op.LONE]";
            case ONE:
                return "[NOT_REQUIRED: ExprUnary.Op.ONE]";
            case TRANSPOSE:
                mustEmitHelperClass = true;
                return "Helper.Transpose(" + x.sub.accept(this) + ")";
            case RCLOSURE:
                mustEmitHelperClass = true;
                return "Helper.RClosure(" + x.sub.accept(this) + ")";
            case CLOSURE:
                mustEmitHelperClass = true;
                return "Helper.Closure(" + x.sub.accept(this) + ")";
            case CARDINALITY:
                return x.sub.accept(this) + ".Count";
            case CAST2INT:
                return "(uint)" + x.sub.accept(this);
            case CAST2SIGINT:
                return "(int)" + x.sub.accept(this);
            case NOOP:
                return x.sub.accept(this);
            default:
                return "[NOT_AN_OP: ExprUnary]";
        }
    }

    /**
     * Emit a string representation of the C# code for a ExprVar.
     *
     * @param x     The ExprVar object.
     * @return      A string representing the equivalent C# code of the ExprVar.
     * @throws Err
     */
    @Override
    public String visit(ExprVar x) throws Err
    {
        return GeneratorHelpers.removeThisPrefix(x.label);
    }

    /**
     * Emit a string representation of the C# code for a Sig.
     *
     * @param x     The Sig object.
     * @return      A string representing the equivalent C# code of the Sig.
     * @throws Err
     */
    @Override
    public String visit(Sig x) throws Err
    {
        return GeneratorHelpers.removeThisPrefix(x.label);
    }

    /**
     * Emit a string representation of the C# code for a Sig.Field.
     *
     * @param x     The Sig.Field object.
     * @return      A string representing the equivalent C# code of the Sig.Field.
     * @throws Err
     */
    @Override
    public String visit(Sig.Field x) throws Err
    {
        return GeneratorHelpers.removeThisPrefix(x.label);
    }

    String sourceSet(Expr node) {

        String source = "[ERROR]";
        try {
            // 0 = Set, 1 = e.ItemX
            String type = node.accept(typeVisitor);

            if (type.length() >= 3 && type.startsWith("ISet"))
                source = node.accept(this);
            else
                source = "(new HashSet<" + type + ">(new " + type + "[] { " + node.accept(this) + " }))";
        }
        catch (Exception e) {
            System.out.println("Error. Unable to get source set: " + e.getMessage());
        }

        return source;
    }

    /**
     * Get a static string representation of the C# code for the helper functions class.
     *
     * @return      A string representatino of the C# code for the helper functions class.
     */
    public static String getHelperClass()
    {
        return "public static class Helper {\n" +
                "  public static ISet<Tuple<L, R>> Closure<L, R>(ISet<Tuple<L, R>> set) {\n" +
                "    var closure = new HashSet<Tuple<L, R>>(set);\n" +
                "    var temp = new HashSet<Tuple<L, R>>();\n" +
                "    do {\n" +
                "      temp.Clear();\n" +
                "      foreach (var a in closure)\n" +
                "        foreach (var b in closure)\n" +
                "          if (a.Item2.Equals(b.Item1))\n" +
                "            if (!closure.Contains(new Tuple<L, R>(a.Item1, b.Item2)))\n" +
                "              temp.Add(new Tuple<L, R>(a.Item1, b.Item2));\n" +
                "      closure.UnionWith(temp);\n" +
                "    }\n" +
                "    while (temp.Count > 0);\n" +
                "    return closure;\n" +
                "  }\n" +
                "  public static ISet<Tuple<L, R>> RClosure<L, R>(ISet<Tuple<L, R>> set) {\n" +
                "    var rclosure = new HashSet<Tuple<L, R>>();\n" +
                "    foreach (var a in set) {\n" +
                "      var aR = new Tuple<L, R>(a.Item1, (R)Convert.ChangeType(a.Item1, typeof(R)));\n" +
                "      var aL = new Tuple<L, R>((L)Convert.ChangeType(a.Item2, typeof(L)), a.Item2);\n" +
                "      if (!rclosure.Contains(aR))\n" +
                "        rclosure.Add(aR);\n" +
                "      if (!rclosure.Contains(aL))\n" +
                "        rclosure.Add(aL);\n" +
                "    }\n" +
                "    rclosure.UnionWith(Closure(set));\n" +
                "    return rclosure;\n" +
                "  }\n" +
                "  public static ISet<Tuple<R, L>> Transpose<L, R>(ISet<Tuple<L, R>> set) {\n" +
                "    var result = new HashSet<Tuple<R, L>>();\n" +
                "    foreach (var e in set)\n" +
                "      result.Add(new Tuple<R, L>(e.Item2, e.Item1));\n" +
                "    return result;\n" +
                "  }\n" +
                "}\n";
    }

    /**
     * Check whether the helper functions class is required to be emitted.
     *
     * @return      A flag indicationg whether the helper functions class is required to be emitted.
     */
    public boolean getMustEmitHelperClass()
    {
        return mustEmitHelperClass;
    }
}

