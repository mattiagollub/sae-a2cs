package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

public class TypeVisitor extends VisitQuery<String> {

    boolean itemTypeOnly = false;

    public TypeVisitor() {

    }

    @Override
    public String visit(Sig x) throws Err {
        return GeneratorHelpers.removeThisPrefix(x.label);
    }

    @Override
    public String visit(ExprBad x) throws Err {
        return "[BAD EXPRESSION]";
    }

    @Override
    public String visit(ExprBadCall x) throws Err {
        return "[BAD CALL]";
    }

    @Override
    public String visit(ExprBadJoin x) throws Err {
        return "[BAD JOIN]";
    }

    @Override
    public String visit(ExprBinary x) throws Err {

        boolean oldItemOnly = itemTypeOnly;
        itemTypeOnly = true;

        switch (x.op) {
            case ARROW:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ANY_ARROW_SOME:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ANY_ARROW_ONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ANY_ARROW_LONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case SOME_ARROW_ANY:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case SOME_ARROW_SOME:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case SOME_ARROW_ONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case SOME_ARROW_LONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ONE_ARROW_ANY:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ONE_ARROW_SOME:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ONE_ARROW_ONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ONE_ARROW_LONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case LONE_ARROW_ANY:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case LONE_ARROW_SOME:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case LONE_ARROW_ONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case LONE_ARROW_LONE:
                return "ISet<Tuple<" + x.left.accept(this) + ", " + x.right.accept(this) + ">>";
            case ISSEQ_ARROW_LONE:
                return "[NOT_REQUIRED:ExprBinary:ISSEQ_ARROW_LONE]";
            case JOIN:
                // The type of the expression is the type of the right expression
                return x.right.accept(this);
            case DOMAIN:
                return "[NOT_REQUIRED:ExprBinary:DOMAIN]";
            case RANGE:
                return "[NOT_REQUIRED:ExprBinary:RANGE]";
            case INTERSECT:
                return x.left.accept(this);
            case PLUSPLUS:
                return "[NOT_REQUIRED:ExprBinary:PLUSPLUS]";
            case PLUS:
                return "ISet<Object>";
            case IPLUS:
                return x.left.accept(this);
            case MINUS:
                return x.left.accept(this);
            case IMINUS:
                return x.left.accept(this);
            case MUL:
                return x.left.accept(this);
            case DIV:
                return x.left.accept(this);
            case REM:
                return x.left.accept(this);
            case EQUALS:
                return "bool";
            case NOT_EQUALS:
                return "bool";
            case IMPLIES:
                return "bool";
            case LT:
                return "bool";
            case LTE:
                return "bool";
            case GT:
                return "bool";
            case GTE:
                return "bool";
            case NOT_LT:
                return "bool";
            case NOT_LTE:
                return "bool";
            case NOT_GT:
                return "bool";
            case NOT_GTE:
                return "bool";
            case SHL:
                return "[NOT_REQUIRED:ExprBinary:SHL]";
            case SHA:
                return "[NOT_REQUIRED:ExprBinary:SHA]";
            case SHR:
                return "[NOT_REQUIRED:ExprBinary:SHR]";
            case IN:
                return "bool";
            case NOT_IN:
                return "bool";
            case AND:
                return "bool";
            case OR:
                return "bool";
            case IFF:
                return "bool";
        }

        itemTypeOnly = oldItemOnly;
        return "[TODO:ExprBinary:???]";
    }

    @Override
    public String visit(ExprList x) throws Err {

        switch (x.op) {
            case DISJOINT:
                return "[NOT_REQUIRED:ExprList:DISJOINT]";
            case TOTALORDER:
                return "[NOT_REQUIRED:ExprList:TOTALORDER]";
            case AND:
                return "bool";
            case OR:
                return "bool";
        }

        return "[TODO:ExprList]";
    }

    @Override
    public String visit(ExprCall x) throws Err {
        return x.fun.returnDecl.accept(this);
    }

    @Override
    public String visit(ExprConstant x) throws Err {

        switch (x.op) {
            case TRUE:
                return "bool";
            case FALSE:
                return "bool";
            case IDEN:
                return "[NOT_REQUIRED:ExprConstant:IDEN]";
            case MIN:
                return "int";
            case MAX:
                return "int";
            case NEXT:
                return "[NOT_REQUIRED:ExprConstant:NEXT]";
            case EMPTYNESS:
                return "[NOT_REQUIRED:ExprConstant:EMPTYNESS]";
            case STRING:
                return "string";
            case NUMBER:
                return "int";
        }

        return "[TODO:ExprConstant]";
    }

    @Override
    public String visit(ExprITE x) throws Err {
        return x.left.accept(this);
    }

    @Override
    public String visit(ExprLet x) throws Err {
        return x.sub.accept(this);
    }

    @Override
    public String visit(ExprQt x) throws Err {
        return "[NOT_REQUIRED:ExprQt]";
    }

    @Override
    public String visit(ExprUnary x) throws Err {
        switch (x.op) {
            case SOMEOF:
                if (itemTypeOnly)
                    return x.sub.accept(this);
                else
                    return "ISet<" + x.sub.accept(this) + ">";
            case LONEOF:
                return x.sub.accept(this);
            case ONEOF:
                return x.sub.accept(this);
            case SETOF:
                if (itemTypeOnly)
                    return x.sub.accept(this);
                else
                    return "ISet<" + x.sub.accept(this) + ">";
            case EXACTLYOF:
                return "[NOT_REQUIRED:ExprUnary:EXACTLYOF]";
            case NOT:
                return "bool";
            case NO:
                return "[NOT_REQUIRED:ExprUnary:NO]";
            case SOME:
                return "[NOT_REQUIRED:ExprUnary:SOME]";
            case LONE:
                return "[NOT_REQUIRED:ExprUnary:LONE]";
            case ONE:
                return "[NOT_REQUIRED:ExprUnary:ONE]";
            case TRANSPOSE:
                return x.sub.accept(this);
            case CLOSURE:
                return x.sub.accept(this);
            case CARDINALITY:
                return "uint";
            case CAST2INT:
                return "uint";
            case CAST2SIGINT:
                return "int";
            case NOOP:
                return x.sub.accept(this);
        };
        return "[TODO:ExprUnary:???]";
    }

    @Override
    public String visit(ExprVar x) throws Err {
        return x.type().toExpr().accept(this);
    }

    @Override
    public String visit(Sig.Field x) throws Err {
        return x.decl().expr.accept(this);
    }
}
