abstract sig FSObject {}

sig File, Dir extends FSObject {}

sig FileSystem {
  live: set FSObject,
  root: Dir & live,
  parent: (live - root) -> one (Dir & live),
  contents: (Dir & live) -> live
}

pred inv[ s: FileSystem ] {
  // A directory is the parent of its contents and vice versa
  s.contents = ~(s.parent)

  // File system is connected
  s.live in s.root.*(s.contents)
}

pred init[ s: FileSystem ] {
  #s.live = 1 
} 

assert initEstablishes {
  all s: FileSystem | init[ s ] => inv[ s ]
}
check initEstablishes for 3 but 1 FileSystem

