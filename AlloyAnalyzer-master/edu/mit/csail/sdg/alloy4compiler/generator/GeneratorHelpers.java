package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;

import java.io.PrintWriter;
import java.util.List;

public class GeneratorHelpers {


    public static boolean isSig(Expr e) {

        Object expr = e;
        while (expr instanceof ExprUnary) {
            expr = ((ExprUnary)expr).sub;
        }

        if (expr instanceof Sig)
            return true;
        else
            return false;
    }

    public static String andConditions(List<String> conditions) {

        String sep = "";
        String contract = "";

        for (String c : conditions) {
            contract += sep + c;
            sep = " && ";
        }

        return contract;
    }

	public static String removeThisPrefix(String s) {
		if (s.contains("/"))
			return s.split("/")[1];
		
		return s;
	}
	
	public static String removeSuffix(String a, String s) {
		if (s.contains(a))
			return s.split(a)[0];
		
		return s;
	}
	
	/**
	 * Remove left and right brackets from a string (only if they match).
	 * 
	 * @param s		the string to be unbracketed
	 * @param bl	the left bracket to be removed
	 * @param br	the right bracket to be removed
	 * @return		the unbracketed string if match found, the original string otherwise
	 */
	public static String unbracket(String s, char bl, char br)
	{
		int last = s.length() - 1;
		return s.charAt(0) == bl && s.charAt(last) == br ? s.substring(1, last) : s;
	}
	
	/**
	 * The default indentation.
	 */
	private static String indentation = "  ";
	
	/**
	 * Set indentation level to a writing stream.
	 * 
	 * @param out	the stream to be written
	 * @param n		the indentation level
	 */
	public static void indent(PrintWriter out, int n)
	{
		for (int i = 0; i < n; ++i)
			out.print(indentation);
	}
}
