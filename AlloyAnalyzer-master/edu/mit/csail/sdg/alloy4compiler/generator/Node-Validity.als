sig Node {
  next: Node
}

assert demo {
   all n: Node | some m: Node | m.next = n
}

check demo for 2
