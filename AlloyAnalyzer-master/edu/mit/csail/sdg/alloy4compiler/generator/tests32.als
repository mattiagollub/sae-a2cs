abstract sig FSObject {
  parent: lone Dir
}

sig File extends FSObject {}

sig Dir extends FSObject {
  contents: set FSObject
}

one sig Root extends Dir {}

// Root is the root
fact { no Root.parent }

// A directory is the parent of its contents
fact inverse1 { all d: Dir, o: d.contents | o.parent = d }

// An object is in the contents of its parent 
fact inverse2 { all o: FSObject | some o.parent => o in o.parent.contents }

// The contents path is acyclic
fact acyclic { no d: Dir | d in d.^contents }

// File system is connected
fact { FSObject in Root.*contents }

//check acyclic for 5

pred isLeave[f: FSObject] {
  f in File
}

assert nonEmptyRoot { !isLeave[ Root ] }
check nonEmptyRoot for 3





