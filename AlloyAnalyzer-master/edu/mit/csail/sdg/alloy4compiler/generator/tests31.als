/*
A song by Doris Day goes:
Everybody loves my baby but my baby don’t love nobody but me
David Gries has pointed out that, from a strictly logical point of view, 
this implies ‘I am my baby’. 
Check this, by formalizing the song as some constraints, 
and Gries’s inference as an assertion. 
Then modify the constraints to express what Doris Day probably meant, 
and show that the assertion now has a counterexample.
*/

sig Person {
	loves: set Person
}

one sig Me extends Person {}

assert song {

}

//run my for 3

check song for 5
