# From Alloy to C#  #

Developed for the Software Architecture and Engineering class, the project is an extension for the Alloy analyzer. The Java software is able to convert an Alloy model to a set of C# classes and code contracts. It also translates Alloy counterexamples to C# objects, so that the violations in the contracts are detected.